#include <vector>
#include <string>
#include <algorithm>
#include <set>
#include <iostream>
#include <fstream>
#pragma once

const int MaxYear = 2020;
const int MaxMonth = 12;
const int MaxDay = 31;

const int MinYear = 1950;
const int MinMonth = 1;
const int MinDay = 1;

const int PerYear = 365;
const int PerMonth = 31;

typedef std::vector<std::pair<std::string, std::string>> dates_vect;

bool IsANumber(const std::string& num);
void InputNumber(const std::string& pMessage, int pMin, int pMax, int &pDigit);
void InputDouble(const std::string& pMessage, double pMin, double pMax, double &pDigit);
bool InputDates(std::string& date, const std::string& message);
bool InputQuery();
void Trim(std::string &str);
void DelSpaces(std::string &str);
bool CheckName(std::string& name);
bool CheckDate(std::string& date);
bool CheckString(std::string& str, std::set<int>& res);
void StartWrite(std::ofstream& file);
void StartScript(std::ofstream& file, std::string dates);
void NotEnoughData(std::ofstream& file);
void InitVectorForMA(std::vector<std::string>& names);
int days_between(const std::string& begin, const std::string& end);


