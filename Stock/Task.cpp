#include "stdafx.h"
#include "Task.h"

using namespace std;

Capital fund;

void PrintMenu()
{
	cout << "\n�������� ��������� ��������.\n";
	cout << " 1 - ���������� ����� �����\n";
	cout << " 2 - ���������� ���������� � ���������� �����\n";
	cout << " 3 - ��������� ������ �� �����\n";
	cout << " 4 - ������ �����\n";
	cout << " 5 - �������� ����\n";
	cout << " --------------------------------------------------\n";
	cout << " 6 - �������� ����� ���������� ���������� ����� ���� ��������\n";
	cout << " 7 - �������� ����� ���������� ���������� ������� ���� ��������\n";
	cout << " --------------------------------------------------\n";
	cout << " 8 - ��������� ���������� ������� ��������� �����\n";
	cout << " 9 - ������� html ���� ��� ��������� ������ � ����������\n";
	cout << " 0 - �����\n";
}
bool less_double(double first, double second)
{
	return first < second;
}
bool more_double(double first, double second)
{
	return first > second;
}

void Add()
{
	string name;
	Share* s = new Share();
	cout << "������� �������� �����.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	s->InputShare(name);
	if (!fund.Add(s))
		cout << "\n����� � ������ ��������� ��� ���� � �����." << endl;
	else
		cout << "\n����� ���� ���������.\n";
}
void AddInfo()
{
	string name, date;
	cout << "������� �������� �����.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	Share* s = fund.Find(name);
	if (s == nullptr)
		cout << "\n������ ����� ����������� � �����.\n";
	else
	{
		while (!InputDates(date, "������� ����.\n"))
			cout << "\n������� �������� ����. ��������� ����.\n\n";
		Price p;
		p.InputPrice();
		pair<string, Price> el;
		el.first = date;
		el.second = p;
		if (s->AddInfo(el))
			cout << "���������� ������ �������.\n";
		else
			cout << "���� �� ���� " << date << " ��� ������� ��� ������ �����.\n";;
	}
}
void GrowingPeriod()
{
	string name;
	cout << "������� �������� �����.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	Share* s = fund.Find(name);
	if (s != nullptr)
	{
		dates_vect dates;
		s->GetPeriod(dates, less_double);
		if (dates.size() == 0) //  || (_first == _second) != 0)
			cout << "\n���� �������� �� �����.\n";
		else
		{
			{
				cout << "\n��� ���������� ����� ���� �������� ������������ �����:\n";
				for (auto i = dates.begin(); i != dates.end(); ++i)
					cout << "\tc\t" << (*i).first << endl << "\t��\t" << (*i).second << endl << endl;
			}
		}
	}
	else
		cout << "������ ����� ����������� � �����.\n";
}
void DropingPeriod()
{
	string name;
	cout << "������� �������� �����.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	Share* s = fund.Find(name);
	if (s != nullptr)
	{
		dates_vect dates;
		s->GetPeriod(dates, more_double);
		if (dates.size() == 0) // || (_first == _second) != 0)
			cout << "\n���� �������� �� ������.\n";
		else
		{
			{
				cout << "\n��� ���������� ������� ���� �������� ������������ �����:\n";
				for (auto i = dates.begin(); i != dates.end(); ++i)
					cout << "\tc\t" << (*i).first << endl << "\t��\t" << (*i).second << endl << endl;
			}
		}
	}
	else
		cout << "������ ����� ����������� � �����.\n";
}

void MovingAverage()
{
	string name, share_name;
	Share* moving_average;
	cout << "������� �������� �����.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	Share* s = fund.Find(name);
	if (s != nullptr)
	{
		share_name = s->GetName();
		switch (s->GetSize())
		{
		case 1: moving_average = s; break;
		case 2: moving_average = s->MovingAverage(2); break;
		default:
		{
			int k;
			InputNumber("������� ������� ���������� �������; 2 <= level <= " + to_string(s->GetSize()), 2, s->GetSize(), k);
			moving_average = s->MovingAverage(k);
			break;
		}
		}
		if (moving_average != nullptr)
			moving_average->PrintShare();
		//delete(moving_average);
	}
	else
		cout << "������ ����� ����������� � �����.\n";
}
void CreateHTMLFile()
{
	string name;
	Share* moving_average;
	cout << "������� �������� �����, ���������� � ������� ��������� ���������� �� �������.\n";
	getline(cin, name);
	while (!CheckName(name))
	{
		cout << "������! ������� �������� ��������.\n";
		cout << "������� �������� �����.\n";
		getline(cin, name);
	}
	Share* s = fund.Find(name);
	if (s == nullptr)
		cout << "������ ����� ����������� � �����.\n";
	else
	{
		/*if (s->GetSize() < 2)
		{
			cout << "������������ ������ ��� ���������� �������.\n";
			return;
		}*/
		ofstream file;
		file.open("index.html");
		StartWrite(file);
		if (s->GetSize() < 2)
		{
			NotEnoughData(file);
			file.close();
			return;
		}
		StartScript(file, s->GetDates());
		vector<string> names;
		names.push_back("Open price");
		names.push_back("Close price");
		names.push_back("Min price");
		names.push_back("Max price");
		names.push_back("#FF3333"); // RED
		names.push_back("#009900"); // GREEN
		names.push_back("#0066FF"); // BLUE
		names.push_back("#990099"); // VIOLET

		vector<string> dates;
		s->GetGraphic(file, names, false, dates);
		cout << "������� �������� �� ������ ���������� ������� ������ �����? y/n\n";
		if (InputQuery())
		{
			switch (s->GetSize())
			{
			case 1: moving_average = s; break;
			case 2: moving_average = s->MovingAverage(2); break;
			case 3: moving_average = s->MovingAverage(2); break;
			default:
			{
				int k;
				//InputNumber("������� ������� ���������� �������; 2 <= level <= " + to_string(s->GetSize()), 2, s->GetSize(), k);
				InputNumber("������� ������� ���������� �������; 2 <= level <= " + to_string(s->GetSize() - 1), 2, s->GetSize() - 1, k);
				moving_average = s->MovingAverage(k);
				break;
			}
			}
			InitVectorForMA(names);
			moving_average->GetGraphic(file, names, true, dates);
			//delete(moving_average);
		}
		file << "		]\n\
					};\n\
				zingchart.render({\n\
					id : 'myChart',\n\
					data : myConfig,\n\
					height : 600,\n\
					width : 1225\n\
				}); \n\
			</script>\n\
		</body>\n\
		</html>";																																																																
		file.close();
	}
}

void Task()
{
	int item;
	string name, date, str;
	Share* moving_average = nullptr;
	string share_name;

	PrintMenu();
	InputNumber("---> ", 0, 9, item);
	while (item)
	{
		switch (item)
		{
		case 1: // ����������
		{
			Add();
			break;
		}
		case 2: // ���������� ����������� � ���������� �����
		{
			AddInfo();
			break;
		}
		case 3: // �������� �� �����
		{
			if (fund.LoadFromFile("fund2.txt"))
				cout << "������ ���� ��������� �� �����.\n";
			else
				cout << "������ �������� ������ �� �����.\n";
			break;
		}
		case 4: // ������
		{
			fund.Print();
			break;
		}
		case 5: // �������
		{
			cout << "�� ������������� ������ �������� ����? y/n" << endl;
			if (InputQuery())
				fund.Clear();
			break;
		}
		case 6: // ���������� ���������� ����� ���� ��������
		{
			GrowingPeriod();
			break;
		}
		case 7: // ���������� ���������� ������� ���� ��������
		{
			DropingPeriod();
			break;
		}
		case 8:  // ���������� �������
		{
			MovingAverage();
			break;
		}
		case 9: // �������� html �����
		{
			CreateHTMLFile();
			break;
		}
		}
		PrintMenu();
		InputNumber("---> ", 0, 9, item);
	}
}
