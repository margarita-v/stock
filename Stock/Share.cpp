#include "stdafx.h"
#include "Share.h"

using namespace std;

typedef pair<string, Price> elem;

void Price::InputPrice()
{
	InputDouble("������� ����������� ���� �����.", 1, 99999, min);
	if (min == 99999)
		cout << "�������� ����������� ��������� ����������� ���� �����.\n������������ ���� ����� 1000000.\n";
	else
		InputDouble("������� ������������ ���� �����.", min + 1, 100000, max);

	InputDouble("������� ���� ��������.", min, max, open);
	InputDouble("������� ���� ��������.", min, max, close);
}

void Price::PrintPrice() const
{
	cout << "\t���� ��������: " << open << endl;
	cout << "\t���� ��������: " << close << endl;
	cout << "\t����������� ����: " << min << endl;
	cout << "\t������������ ����: " << max << endl;
}

void Share::InputShare(string pName)
{
	string str;
	string date;
	Price price;
	if (pName == "")
	{
		cout << "������� �������� �����.\n";
		getline(cin, str);
		while (!CheckName(str))
		{
			cout << "������! ������� �������� ��������.\n";
			cout << "������� �������� �����.\n";
			getline(cin, str);
		}
		name = str;
	}
	else
		name = pName;
	while (!InputDates(date, "������� ����.\n"))
		cout << "\n������� �������� ����. ��������� ����.\n\n";

	cout << "������� ���������� � ����.\n";
	price.InputPrice();
	pair<string, Price> elem;
	elem.first = date;
	elem.second = price;
	AddInfo(elem);
}

void Share::PrintShare()
{
	cout << endl;
	cout << "�������� �����: " << name << endl;
	for (auto i = info.begin(); i != info.end(); ++i)
	{
		cout << "����: \t" << (*i).first << endl;
		(*i).second.PrintPrice();
		cout << "--------------------------------------\n";
	}
	cout << endl;
}

bool Share::Find(string& date)
{
	bool pFind = false;
	auto i = info.begin();
	while ((!pFind) && i != info.end())
	{
		pFind = (*i).first == date;
		if (pFind)
			break;
		i++;
	}
	return pFind;
}

bool Share::AddInfo(pair<string, Price>& elem)
{
	if (CheckDate(elem.first) && !Find(elem.first) && elem.second.CheckPrice())
	{
		info.insert(elem);
		return true;
	}
	return false;
}

bool date_compare(const string& d1, const string& d2) 
{
	string::size_type sz;
	string s = d1;

	int first_day = stoi(s, &sz);
	s = s.substr(sz + 1);
	int first_month = stoi(s, &sz);
	s = s.substr(sz + 1);
	int first_year = stoi(s, &sz);

	s = d2;
	int second_day = stoi(s, &sz);
	s = s.substr(sz + 1);
	int second_month = stoi(s, &sz);
	s = s.substr(sz + 1);
	int second_year = stoi(s, &sz);
	return (first_year < second_year ||
		first_year == second_year && first_month < second_month ||
		first_year == second_year && first_month == second_month && first_day < second_day);
};

void Share::GetPeriod(dates_vect& dates, bool(*compare)(double first, double second))
{
	dates.clear();
	if (info.size() > 1)
	{
		int count = 0, max_count = 1;
		string first_date = (*info.begin()).first;
		string second_date = first_date;

		string f_date = first_date;
		string s_date = second_date;

		auto first = info.begin();
		auto second = first;
		second++;

		int count_days = 1;

		PrintShare();
		for (; second != info.end(); ++second, ++first)
			if (compare((*first).second.GetOpenPrice(), (*second).second.GetOpenPrice()))
			{
				//count++;
				s_date = (*second).first;
				count = days_between(f_date, s_date);
			}
			else
			{
				if (count == count_days)
				{
					dates.push_back(make_pair(f_date, s_date));
					count = 1;
				}
				else
					if (count > count_days && date_compare(f_date, s_date))
					{
						count_days = count;
						first_date = f_date;
						second_date = s_date;

						dates.clear();
						dates.push_back(make_pair(first_date, second_date));
						count = 1;
					}
				f_date = (*second).first;
			}
		// �������� ���������� ����������
		if (count == count_days)
		{
			dates.push_back(make_pair(f_date, s_date));
			count = 1;
		}
		else
			if (count > count_days && date_compare(f_date, s_date))
			{
				count_days = count;
				first_date = f_date;
				second_date = s_date;

				dates.clear();
				dates.push_back(make_pair(first_date, second_date));
				count = 1;
			}
	}
}

Share* Share::MovingAverage(const unsigned& k)
{
	if (k <= info.size() && k > 0)
	{
		Share* result = new Share();
		auto series_begin = info.begin(), i = series_begin;
		while (i != info.end())
		{
			Price p;
			string date;
			double open = 0, close = 0, min = 0, max = 0;
			for (unsigned num = 1; i != info.end() && num <= k; ++i, ++num)
			{
				open += (*i).second.GetOpenPrice();
				close += (*i).second.GetClosePrice();
				min += (*i).second.GetMinPrice();
				max += (*i).second.GetMaxPrice();
				date = (*i).first;
			}
			open /= k;
			close /= k;
			min /= k;
			max /= k;

			p.SetOpenPrice(open);
			p.SetClosePrice(close);
			p.SetMaxPrice(max);
			p.SetMinPrice(min);

			pair<string, Price> elem;
			elem.first = date;
			elem.second = p;

			result->AddInfo(elem);
			series_begin++;
			i = series_begin;
		}
		return result;
	}
	return nullptr;
}

void Share::GetGraphic(ofstream& file, const vector<string>& names, bool for_ma, vector<string>& _dates)
{
	if (names.size() != 8)
		return;

	vector<double> open;
	vector<double> close;
	vector<double> min;
	vector<double> max;
	vector<string> dates;

	GetAllPrices(open, close, min, max, dates);

	if (!for_ma)
	{
		_dates = dates;
		// OPEN
		file << "{\n\
			\"values\":[";
		string str;
		for (auto i = open.begin(); i != open.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
				\"text\" : \"" << names[0] << "\",\n\
				\"line-color\" : \"" << names[4] << "\",\n\
				\"marker\" : {\n\
				\"background-color\":\"" << names[4] << "\",\n\
				\"border-color\" : \"" << names[4] << "\"\n\
			}\n\
		},\n";

		// CLOSE
		file << "		{\n\
			\"values\":[";
		str.clear();
		for (auto i = close.begin(); i != close.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
				\"text\" : \"" << names[1] << "\",\n\
				\"line-color\" : \"" << names[5] << "\",\n\
				\"marker\" : {\n\
				\"background-color\":\"" << names[5] << "\",\n\
					\"border-color\" : \"" << names[5] << "\"\n\
			}\n\
		},\n";

		// MIN
		file << "		{\n\
			\"values\":[";
		str.clear();
		for (auto i = min.begin(); i != min.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
				\"text\" : \"" << names[2] << "\",\n\
				\"line-color\" : \"" << names[6] << "\",\n\
				\"marker\" : {\n\
				\"background-color\":\"" << names[6] << "\",\n\
					\"border-color\" : \"" << names[6] << "\"\n\
			}\n\
		},\n";

		// MAX
		file << "		{\n\
			\"values\":[";
		str.clear();
		for (auto i = max.begin(); i != max.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
				\"text\" : \"" << names[3] << "\",\n\
				\"line-color\" : \"" << names[7] << "\",\n\
				\"marker\" : {\n\
				\"background-color\":\"" << names[7] << "\",\n\
					\"border-color\" : \"" << names[7] << "\"\n\
			}\n\
		}\n";
	}
	else // for_ma
	{
		dates = _dates;
		// OPEN
		file << ",\n{\n\
			\"values\":[";
		string str;
		unsigned count = 0;
		for (; count < (dates.size() - open.size()); count++)
			str += "null,";
		for (auto i = open.begin(); i != open.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
			\"text\" : \"" << names[0] << "\",\n\
			\"line-color\" : \"" << names[4] << "\",\n\
			\"marker\" : {\n\
				\"background-color\":\"" << names[4] << "\",\n\
				\"border-color\" : \"" << names[4] << "\"\n\
			}\n\
		},\n";

		// CLOSE
		file << "		{\n\
			\"values\":[";
		str.clear();
		count = 0;
		for (; count < (dates.size() - open.size()); count++)
			str += "null,";
		for (auto i = close.begin(); i != close.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
			\"text\" : \"" << names[1] << "\",\n\
			\"line-color\" : \"" << names[5] << "\",\n\
			\"marker\" : {\n\
				\"background-color\":\"" << names[5] << "\",\n\
				\"border-color\" : \"" << names[5] << "\"\n\
			}\n\
		},\n";

		// MIN
		file << "		{\n\
			\"values\":[";
		str.clear();
		count = 0;
		for (; count < (dates.size() - open.size()); count++)
			str += "null,";
		for (auto i = min.begin(); i != min.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
			\"text\" : \"" << names[2] << "\",\n\
			\"line-color\" : \"" << names[6] << "\",\n\
			\"marker\" : {\n\
				\"background-color\":\"" << names[6] << "\",\n\
				\"border-color\" : \"" << names[6] << "\"\n\
			}\n\
		},\n";

		// MAX
		file << "		{\n\
			\"values\":[";
		str.clear();
		count = 0;
		for (; count < (dates.size() - open.size()); count++)
			str += "null,";
		for (auto i = max.begin(); i != max.end(); ++i)
			str += to_string(*i) + ",";
		str.pop_back();
		file << str << "],\n\
			\"text\" : \"" << names[3] << "\",\n\
			\"line-color\" : \"" << names[7] << "\",\n\
			\"marker\" : {\n\
				\"background-color\":\"" << names[7] << "\",\n\
				\"border-color\" : \"" << names[7] << "\"\n\
			}\n\
		}\n";
	}
}

void Share::GetAllPrices(vector<double>& open, vector<double>& close, vector<double>& min, vector<double>& max, vector<string>& dates)
{
	for (auto i = info.begin(); i != info.end(); ++i)
	{
		open.push_back((*i).second.GetOpenPrice());
		close.push_back((*i).second.GetClosePrice());
		min.push_back((*i).second.GetMinPrice());
		max.push_back((*i).second.GetMaxPrice());
		dates.push_back((*i).first);
	}
}
