#include "stdafx.h"
#include "Methods.h"

using namespace std;

bool IsANumber(const string& s)
{
	int i;
	int len = s.length();
	for (i = 0; i < len && isdigit((unsigned char)s[i]); i++);
	return (len) && (i == len);
}

void Trim(string &str)
{
	const auto strBegin = str.find_first_not_of(" \t");
	if (strBegin != string::npos)
	{
		const auto strEnd = str.find_last_not_of(" \t");
		const auto strRange = strEnd - strBegin + 1;
		str = str.substr(strBegin, strRange);
	}
	else
		str = "";

	string result = "";
	for (unsigned int i = 0; i < str.length(); i++)
		result += (unsigned char)tolower(str.at(i));
	str = result;
}

void DelSpaces(string &str)
{
	string result = "";
	Trim(str);
	unsigned i = 0;
	while (i < str.length())
	{
		while (str[i] != ' ' && i < str.length())
		{
			result.push_back(str[i]);
			i++;
		}
		if (str[i] == ' ' && i < str.length())
			result.push_back(' ');
		while (str[i] == ' ' && i < str.length())
			i++;
	}
	str = result;
}

bool CheckDigit(string& str)
{
	Trim(str);
	bool ok = str.size() > 0;
	return ok;
}

void InputNumber(const string& pMessage, int pMin, int pMax, int &pDigit)
{
	string num;
	bool correct = false;
	cout << pMessage << std::endl;
	getline(cin, num);
	correct = CheckDigit(num) && IsANumber(num);
	if (correct)
	{
		pDigit = stoi(num);
		correct = (pDigit >= pMin && pDigit <= pMax);
	}
	while (!correct)
	{
		cout << "������! ������� ����� �� " << pMin << " �� " << pMax << endl;
		getline(cin, num);
		correct = CheckDigit(num) && IsANumber(num);
		if (correct)
		{
			pDigit = stoi(num);
			correct = (pDigit >= pMin && pDigit <= pMax);
		}
	}
}

void InputDouble(const string& pMessage, double pMin, double pMax, double &pDigit)
{
	string num;
	bool correct = true;
	do
	{
		if (!correct)
			cout << "������! ������� ����� �� " << pMin << " �� " << pMax << endl;
		cout << pMessage << std::endl;
		getline(cin, num);
		correct = CheckDigit(num);
		int cnt = 0;
		if (correct)
		{
			correct = num.begin() != num.end() && (*num.begin() == '+' || *num.begin() == '-' || *num.begin() >= '0' && *num.begin() <= '9');
			if (*num.begin() == ',') {
				correct = true;
				++cnt;
			}
			for (string::iterator i = ++num.begin(); i != num.end() && correct; ++i)
			{
				correct = (*i >= '0' && *i <= '9') || *i == ',';
				if (*i == ',')
					++cnt;
				correct &= cnt <= 1;
			}
			correct = correct && !(num.size() == 1 && !isdigit((unsigned char)*num.begin()));
			if (correct)
			{
				pDigit = stof(num);
				correct = (pDigit >= pMin && pDigit <= pMax);
			}
		}
	} while (!correct);
}

bool CheckName(string& name)
{
	DelSpaces(name);
	bool ok = name.size() > 0;
	return ok;
}

bool CheckDate(string& date)
{
	DelSpaces(date);
	string s = date;

	bool isOkNumber = true;
	if (s.length() != 10)
		return false;
	s[10] = '\0';

	isOkNumber =
		(*(s.begin() + 2) == '.') && (*(s.begin() + 5) == '.') ||
		(*(s.begin() + 2) == ',') && (*(s.begin() + 5) == ',') ||
		(*(s.begin() + 2) == '/') && (*(s.begin() + 5) == '/');

	string::iterator it = s.begin();
	for (; it != s.end() && isOkNumber; ++it) {
		if (s.begin() + 2 != it && s.begin() + 5 != it)
			isOkNumber = ((*it) <= '9') && ((*it) >= '0');
	}

	if (!isOkNumber || s.begin() + 10 != it)
		return false;

	int d, m, y;
	string::size_type sz;

	d = stoi(s, &sz);
	s = s.substr(sz + 1);
	m = stoi(s, &sz);
	s = s.substr(sz + 1);
	y = stoi(s, &sz);

	return (d >= MinDay && d <= MaxDay && m >= MinMonth && m <= MaxMonth && y >= MinYear && y <= MaxYear);
}

bool InputDates(string& date, const string& message)
{
	string s;
	cout << message << endl; 
	cout << "������ ����: ��.��.����" << endl;
	cout << MinDay << "\t< �� <\t" << MaxDay << endl;
	cout << MinMonth << "\t< �� <\t" << MaxMonth << endl;
	cout << MinYear << "\t<����<\t" << MaxYear << endl;
	cout << "--> ";
	getline(cin, s);

	date = s;

	return CheckDate(date);
}

bool InputQuery()
{
	string s;
	bool correct = true;
	do
	{
		_flushall();
		getline(std::cin, s);
		Trim(s);
		correct = (s.size() == 1) && (s[0] == 'y' || s[0] == 'Y' || s[0] == '�' || s[0] == '�' || s[0] == 'n' || s[0] == 'N' || s[0] == '�' || s[0] == '�');
		if (!correct)
			cout << "������! ��������� ����.\n";
	} while (!correct);

	return (s[0] == 'y' || s[0] == 'Y' || s[0] == '�' || s[0] == '�');
}

bool CheckString(string& str, set<int>& res)
{
	DelSpaces(str);
	if (str.size() == 0)
		return false;
	string num;
	auto i = str.begin();
	while (i != str.end())
	{
		while (i != str.end() && *i != ' ')
		{
			num.push_back(*i);
			++i;
		}

		if (IsANumber(num))
		{
			int number = stoi(num);
			if (number > 0 && number < 5)
				res.insert(stoi(num));
		}
		else
			return false;

		num.clear();
		if (i != str.end())
			++i; // ������� �������
	}
	if (res.size() == 0)
		return false;
	return true;
}

void StartWrite(ofstream& file)
{
	file << "<!DOCTYPE html>\n\
	\n\
	<html>\n\
		<head>\n\
			<title>Stock</title>\n\
			<script src=\"zingchart_branded_version/zingchart_2.3.2/zingchart.min.js\"></script>\n\
			<script>\n\
			zingchart.MODULESDIR = \"zingchart_branded_version/zingchart_2.3.2/modules/\";\n\
    		ZC.LICENSE = [\"569d52cefae586f634c54f86dc99e6a9\",\"ee6b7db5b51705a13dc2339db3edaf6d\"];</script>\n\
			<meta http-equiv=\"Content-Type\" content=\"text/html\" charset = \"UTF-8\">\n\
		</head>\n\
    	<body>\n\
    		<div id='myChart'></div>\n";
}
void StartScript(ofstream& file, string dates)
{
	file << "<script type = \"text/javascript\">\n\
			var myConfig = {\n\
			\"background-color\":\"white\",\n\
			\"type\" : \"line\",\n\
			\"title\" : {\n\
				\"text\":\"Information about the fund\",\n\
				\"color\" : \"#333\",\n\
				\"background-color\" : \"white\",\n\
				\"width\" : \"60%\",\n\
				\"text-align\" : \"left\",\n\
			},\n\
			\"legend\" : {\n\
				\"layout\":\"x1\",\n\
				\"margin-top\" : \"5%\",\n\
				\"border-width\" : \"0\",\n\
				\"shadow\" : false,\n\
				\"marker\" : {\n\
					\"cursor\":\"hand\",\n\
					\"border-width\" : \"0\"\n\
				},\n\
				\"background-color\" : \"white\",\n\
				\"item\" : {\n\
					\"cursor\":\"hand\"\n\
				},\n\
				\"toggle-action\" : \"remove\"\n\
			},\n";

	file << "		\"scaleX\":{\n\
			\"labels\":[" << dates << "\t\t\t]\n";
	
	file << "	},\n\
		\"scaleY\":{\n\
			\"line-color\":\"#333\"\n\
		},\n\
		\"tooltip\" : {\n\
			\"text\":\"%t: %v in date number %k\"\n\
		},\n\
		\"plot\" : {\n\
			\"marker\" : {\n\
				\"size\":2\n\
			},\n\
			\"selection-mode\" : \"multiple\",\n\
			\"background-mode\" : \"graph\",\n\
			\"selected-state\" : {\n\
				\"line-width\":4\n\
			},\n\
			\"background-state\" : {\n\
				\"line-color\":\"#eee\",\n\
				\"marker\" : {\n\
					\"background-color\":\"none\"\n\
				}\n\
			}\n\
		},\n\
		\"plotarea\":{\n\
			\"margin\":\"15% 25% 10% 7%\"\n\
		},\n\
		\"series\":[";
}
void NotEnoughData(ofstream& file)
{
	file << "<p>Not enough data to draw the graph!.</p>\n\
			</body>\n\
		</html>";
}

void InitVectorForMA(vector<string>& names)
{
	names.clear();
	names.push_back("MA Open price");
	names.push_back("MA Close price");
	names.push_back("MA Min price");
	names.push_back("MA Max price");
	names.push_back("#FF7F00"); // RED
	names.push_back("#00FF00"); // GREEN
	names.push_back("#003399"); // BLUE
	names.push_back("#FB9A99"); // VIOLET
}

int days_between(const string& begin, const string& end) 
{
	string::size_type sz;
	string s = begin;

	int first_day = stoi(s, &sz);
	s = s.substr(sz + 1);
	int first_month = stoi(s, &sz);
	s = s.substr(sz + 1);
	int first_year = stoi(s, &sz);

	s = end;
	int second_day = stoi(s, &sz);
	s = s.substr(sz + 1);
	int second_month = stoi(s, &sz);
	s = s.substr(sz + 1);
	int second_year = stoi(s, &sz);

	int res = (second_year - first_year) * PerYear + (second_month - first_month) * PerMonth + (second_day - first_day) + 1;
	if (res < 0)
		res = -res;
	return res;
}