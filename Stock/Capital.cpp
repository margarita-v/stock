#include "stdafx.h"
#include "Capital.h"

using namespace std;

typedef pair<string, Price> elem;

Capital::Capital()
{
	ShareList.reserve(0);
}

Capital::~Capital()
{
	Clear();
}

Share* Capital::Find(std::string& pName)
{
	DelSpaces(pName);
	bool pFind = false;
	auto i = ShareList.begin();
	while ((!pFind) && i != ShareList.end())
	{
		pFind = (*i)->GetName() == pName;
		if (pFind)
			break;
		i++;
	}
	if (pFind)
	{
		Share* res = (*i);
		return res;
	}
	else
		return nullptr;
}

bool Capital::Add(Share* pShare)
{
	bool find;
	std::string name = pShare->GetName();
	Share* el = Find(name);
	find = el != nullptr;
	if (!find)
		ShareList.push_back(pShare);
	return !find; 
}

void Capital::Print()
{
	vect::iterator i;
	if (ShareList.size())
		for (i = ShareList.begin(); i != ShareList.end(); ++i)
			(*i)->PrintShare();
	else
		cout << "\n���� ����� ����." << std::endl;
}

/*bool Capital::FindGrowingPeriod(string& name, string& first_date, string& second_date, double& first_price, double& second_price)
{
	Share* share = Find(name);
	if (share != nullptr)
	{
		//share->GetGrowingPeriod(first_date, second_date, first_price, second_price);
		return true;
	}
	return false;
}

bool Capital::FindDropingPeriod(string& name, string& first_date, string& second_date, double& first_price, double& second_price)
{
	Share* share = Find(name);
	if (share != nullptr)
	{
		//share->GetDropingPeriod(first_date, second_date, first_price, second_price);
		return true;
	}
	return false;
}*/

void Capital::Clear()
{
	while (ShareList.size() > 0) {
		delete ShareList.back();
		ShareList.pop_back();
	}
}

bool Capital::LoadFromFile(const string& filename)
{
	ifstream infile;

	Clear();

	infile.open(filename);
	bool ok = true;
	string str;
	string name;

	while (!infile.eof() && ok)
	{
		getline(infile, name);
		if (!infile.eof() && CheckName(name))
		{
			getline(infile, str);
			Share* s = Find(name);
			if (s != nullptr)
				return false;
			s = new Share(name);
			ShareList.push_back(s);
			while (!infile.eof() && str != "*" && ok)
			{
				unsigned i = 0;
				string date;
				string open, close, min, max;
				DelSpaces(str);
				ok = i < str.size();
				while (ok && str[i] != ' ')
				{
					date += str[i];
					i++;
					ok = i < str.size();
				}
				i++;
				ok = CheckDate(date) && (i < str.size());
				while (ok && str[i] != ' ')
				{
					open += str[i];
					i++;
					ok = i < str.size();
				}
				i++;
				ok = ok && (i < str.size());
				while (ok && str[i] != ' ')
				{
					close += str[i];
					i++;
					ok = i < str.size();
				}
				i++;
				ok = ok && (i < str.size());
				while (ok && str[i] != ' ')
				{
					min += str[i];
					i++;
					ok = i < str.size();
				}
				i++;
				ok = ok && (i < str.size());
				while (i < str.size() && str[i] != ' ')
				{
					max += str[i];
					i++;
				}
				if (ok)
				{
					try
					{
						double op = stof(open), cl = stof(close), _min = stof(min), _max = stof(max);
						Price p(op, cl, _min, _max);
						elem el = make_pair(date, p);
						ok = s->AddInfo(el);
					}
					catch (bad_exception e)
					{
						cerr << e.what();
						ok = false;
					}
				}
				getline(infile, str);
			} // while (!infile.eof() && str != "*" && ok)
			if (s->GetSize() == 0)
			{
				Clear();
				return false;
			}
		} // if (!infile.eof && CheckName(name))
		else
			ok = false;
	} // while (!infile.eof && ok)
	if (!ok)
	{
		Clear();
		infile.close();
		return false;
	}
	infile.close();
	return true;
}

// ����� ����� ������ "���������������� ��� ��� Windows"
// �� ������� � ����� ��������( ��� ��������������� �� �++ ).
// ����� ����������� ����� - SFML, OpenGL 