#include "Methods.h"
#pragma once

class Price
{
	double open;
	double close;
	double min;
	double max;
public:
	Price(double pOpen = 0, double pClose = 0, double pMin = 0, double pMax = 0) : open(pOpen), close(pClose), min(pMin), max(pMax) { }
	~Price() { }
	double GetOpenPrice() const { return open; }
	double GetClosePrice() const { return close; }
	double GetMinPrice() const { return min; }
	double GetMaxPrice() const { return max; }

	void SetOpenPrice(const double& value)
	{
		if (value <= 0 || value < min && min != 0 && value > max && max != 0)
			throw new std::bad_exception("�������� �������� ���� ��������.");
		open = value;
	}
	void SetClosePrice(const double& value)
	{
		if (value <= 0 || value < min && min != 0 && value > max && max != 0)
			throw new std::bad_exception("�������� �������� ���� ��������.");
		close = value;
	}
	void SetMinPrice(const double& value)
	{
		if (value <= 0 || value >= max && max != 0)
			throw new std::bad_exception("�������� �������� ����������� ����.");
		min = value;
	}
	void SetMaxPrice(const double& value)
	{
		if (value <= 0 || value <= min && min != 0)
			throw new std::bad_exception("�������� �������� ������������ ����.");
		max = value;
	}

	void InputPrice();
	void PrintPrice() const;
	bool CheckPrice() const 
	{
		return open > 0 && close > 0 && open >= min && open <= max && close >= min && close <= max &&
			min > 0 && max > 0 && min < max;
		//return GetOpenPrice() > 0 && GetClosePrice() > 0 &&
			//GetMinPrice() > 0 && GetMinPrice() < GetMaxPrice() &&
			//GetMaxPrice() > 0 && GetMaxPrice() > GetMinPrice();
	}

	// STRICT WEAK ORDERING

	bool operator ==(const Price& p) const 
	{
		return (p.GetOpenPrice() == open) && (p.GetClosePrice() == close)
			&& (p.GetMinPrice() == min) && (p.GetMaxPrice() == max);
	}
	bool operator !=(const Price& p) const
	{
		return !(*this == p);
	}
	bool operator >(const Price& p) const
	{
		return !(*this < p) && !(*this == p);
	}
	bool operator <(const Price& p) const
	{
		return open < p.GetOpenPrice() ||
			open == p.GetOpenPrice() && close < p.GetClosePrice() ||
			open == p.GetOpenPrice() && close == p.GetClosePrice() && max < p.GetMaxPrice() ||
			open == p.GetOpenPrice() && close == p.GetClosePrice() && max == p.GetMaxPrice() && min < p.GetMinPrice();
	}
};

struct elem_compare {
	bool operator() (const std::pair<std::string, Price>& el1, const std::pair<std::string, Price>& el2) const
	{
		if (!(el1.second.CheckPrice()) || !(el2.second.CheckPrice()))
			throw std::bad_exception("�������� �������� ����.");
		std::string::size_type sz;
		std::string s = el1.first;

		int first_day = stoi(s, &sz);
		s = s.substr(sz + 1);
		int first_month = stoi(s, &sz);
		s = s.substr(sz + 1);
		int first_year = stoi(s, &sz);

		s = el2.first;
		int second_day = stoi(s, &sz);
		s = s.substr(sz + 1);
		int second_month = stoi(s, &sz);
		s = s.substr(sz + 1);
		int second_year = stoi(s, &sz);

		return (first_year < second_year ||
			first_year == second_year && first_month < second_month ||
			first_year == second_year && first_month == second_month && first_day < second_day ||
			first_year == second_year && first_month == second_month && first_day == second_day && el1.second < el2.second);
	}
};

class Share
{
	std::string name;
	std::set<std::pair<std::string, Price>, elem_compare> info;
	void GetAllPrices(std::vector<double>& open, std::vector<double>& close, std::vector<double>& min, std::vector<double>& max, std::vector<std::string>& dates);
public:
	Share(std::string pName = "") : name(pName), info() { }
	~Share() { }
	std::string GetName() { return name; }
	bool Find(std::string& date);
	bool AddInfo(std::pair<std::string, Price>& elem);

	void GetPeriod(dates_vect& dates, bool(*compare)(double first, double second));

	Share* MovingAverage(const unsigned& k);
	void GetGraphic(std::ofstream& file, const std::vector<std::string>& names, bool for_ma, std::vector<std::string>& _dates);

	void InputShare(std::string pName = "");
	void PrintShare();

	int GetSize() const { return info.size(); }
	std::string GetDates() const
	{
		std::string res;
		for (auto i = info.begin(); i != info.end(); ++i)
			res += "\"" + (*i).first + "\",";
		res.pop_back();
		res += "\n";
		return res;
	}
};

