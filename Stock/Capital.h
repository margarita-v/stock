#include "Share.h"
#pragma once

typedef std::vector<Share*> vect;
class Capital
{
	vect ShareList;
public:
	Capital();
	~Capital();

	bool Add(Share* pShare);
	Share* Find(std::string& pName);
	void Print();
	int Size() { return ShareList.size(); }

	//bool FindGrowingPeriod(std::string& name, std::string& first_date, std::string& second_date, double& first_price, double& second_price);
	//bool FindDropingPeriod(std::string& name, std::string& first_date, std::string& second_date, double& first_price, double& second_price);

	//Share* SimpleMovingAverage(std::string& name, const int& k);

	bool LoadFromFile(const std::string& filename);
	void Clear();
};

